# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Single Cell Genomics
* MiSeq data analysis to identify novel clonotypes

### How do I get set up? ###

* Metadata is contained in the Sample Sheet
* Raw Fastq data is available from the researcher (too large to load here)
* Output tables are in tab-delmited text format for easy sorting or upload into excel.

### Contribution guidelines ###

* Custom Code provided by Dr. Adelaide Rhodes, Vermont Integrated Genomics Resource, copyright@2018
* Please cite the code in any downstream applications.

### Who do I talk to? ###

* Repo owner or admin: arhodes@uvm.edu
* PI:  Jonathan.Boyson@uvm.edu
* PI:  Somen-Kumar.Mistri@uvm.edu